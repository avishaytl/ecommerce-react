import React, { useEffect, useRef, useState } from 'react'; 
import './App.css';
import styled from 'styled-components';
import { Autocomplete, TextField } from '@mui/material'; 
import { BiFilterAlt, BiSort, BiImage, BiCart } from 'react-icons/bi';
import { IoIosArrowBack } from 'react-icons/io';
import { FiDelete } from 'react-icons/fi';
import hat from './styles/images/h.jpg';
import mask from './styles/images/m.jpg';
import shoes from './styles/images/s.jpg';
import shirt from './styles/images/t0.jpg'; 
import useDraggableScroll from 'use-draggable-scroll';
import {BrowserRouter as Router, Routes, Route, useNavigate } from "react-router-dom";
import { findDOMNode } from 'react-dom';

const MainView = styled.div`
    width: 80vw;
    max-width: 1400px;
    height: 100vh;
    max-height: 100vh; 
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start; 
`
const ViewContainer = styled.div` 
    overflow-y: auto;
    min-width: 100%; 
`
const GridView = styled.div`  
    width: 100%; 
    display: grid;
    justify-content: center;
    align-content: center;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr; 
    row-gap: 20px;
    column-gap: 20px;  
    padding: 10px;  
    padding-top: 50px;
`

const ItemView = styled.div`  
  min-height: 250px;
  max-width: 150px;
  min-width: 100%;  
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow: hidden;
  border: 1px solid #cecece;
  cursor: pointer;
  border-radius: 10px;
  &:hover img{
    transform: scale(1.1);
  }
  img{
    transition: transform 0.5s; 
  }
`
const IconTitle = styled.p<any>` 
  margin-top: 0px;
  text-align: center; 
  max-width: 100%;
  overflow-wrap: break-word;
  color: ${props => props.color ? props.color : '#fff'};
  font-size: ${props => props.size ? props.size : '14px'};
`

const HeaderView = styled.div<any>`  
  height: 150px; 
  max-width: 1400px;
  width: 100%;  
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  
  position: -webkit-sticky;
  position: sticky;
  background: ${props => props.background};
  border-bottom-width: 2px;
  border-bottom-color: #cecece;
  border-bottom-style: solid;
`

const HeaderChild = styled.div<any>`  
  min-height: 75px; 
  width: 100%;  
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;  
  background: ${props => props.background};
  overflow-x: auto;
  overflow-y: hidden;
`

const HeaderChildItem = styled.div<any>`   
  padding: 10px;
  width: ${props => props.width}; 
  display: flex;
  flex-direction: ${props => props.direction ? props.direction : 'row'};
  align-items: center;
  justify-content: ${props => props.content ? props.content : 'space-between'}; 
  background: ${props => props.background};
`

const FooterView = styled.div<any>`  
  height: 100px; 
  max-width: 1400px;
  width: 100%;  
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  
  position: -webkit-sticky;
  position: sticky;
  background: ${props => props.background};
  border-top-width: 2px;
  border-top-color: #cecece;
  border-top-style: solid; 
`
const FooterChild = styled.div<any>`  
  min-height: 50px; 
  width: 100%;  
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;  
  background: ${props => props.background}; 
`

const TableView = styled.div`  
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center; 
    width: 100%; 
    min-height: calc(100vh - 250px);   
`
const TableScroll = styled.div`  
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: center; 
    width: 100%;   
    overflow-y: auto;  
    overflow-x: hidden; 
`
const TableInfoCol = styled.div<any>`  
    width: ${props => props.width ? props.width : '250px'};  
    background: ${props => props.background ? props.background : '#4e4b4f'}; 
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start; 
    overflow-x: ${props => props.overflow ? 'auto' : 'hidden'}; 
    overflow-y: hidden; 
`
const TableInfoColHeader = styled.div<any>`  
    width: ${props => props.width ? props.width : '250px'};  
    height: 60px; 
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    color: #fff;
    font-size: 14px; 
    overflow-x: ${props => props.overflow ? 'auto' : 'hidden'}; 
    overflow-y: hidden; 
`
const TableInfoColHeaderChild = styled.div<any>`  
    width: 250px;  
    height: 50%;  
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: ${props => props.content ? props.content : 'space-between'}; 
    color: #fff;
    font-size: 14px;
    background: ${props => props.background ? props.background : '#4e4b4f'};
    padding: 10px; 
`
const TableInfoColView = styled.div<any>`   
    width: ${props => props.width ? props.width : '250px'};  
`
const TableInfoColChild = styled.div<any>`  
    width: 250px;  
    height: 50px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between; 
    background: ${props => props.background}; 
    padding: 0 5px 0 5px;
    border-bottom-width: 1px;
    border-bottom-color: #cecece;
    border-bottom-style: solid;
`
const TableSizeHeader = styled.div<any>` 
    max-height: ${props => props.maxHeight ? props.maxHeight : 'auto'}; 
    display: flex;
    flex-direction: row;
    align-items: flex-end;  
` 
const KeyboardItem = styled.div`  
    flex: 1;
    height: 100%; 
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;   
`
const KeyboardItemTitle = styled.div`   
    font-weight: 500;
    padding: 15px; 
    cursor: pointer;
    transition: background 0.5s ease;
    &:hover{
      background: #cecece;
    }
`
const FooterTotalView = styled.div`   
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;  
`
const HeaderChildImg = styled.div<any>`   
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;  
    min-width: 75px;
    height: 75px; 
    background: ${props => props.background ? props.background : '#fff'};
`
const TableHeader = styled.div<any>`    
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;  
    overflow: hidden;
    min-height: 60px;
    max-width: 100%;
    min-width: 100%;
    background: blue;
`

const json = [
  { img: hat,inv: 'מלאי נוכחי - 40230', desc: 'כובים במידות שונות בלי קשר לכלום', label: 'כובעים', id: 1 },
  { img: mask,inv: 'מלאי נוכחי - 2343', desc: 'מסכות חדשות לקורונה קונספירציה וכדומה', label: 'מסכות',id: 2 },
  { img: shoes,inv: 'Feather_shopping-cart', desc: 'ShoppingCartOutlined', label: 'Shopping Cart Outlined',id: 3 },
  { img: shirt,inv: 'מלאי נוכחי - 24213', desc: 'חולצות שונות במידות שונות מבד שונה', label: 'חולצות',id: 4 },
  { img: hat,inv: 'מלאי נוכחי - 40230', desc: 'כובים במידות שונות בלי קשר לכלום', label: 'כובעים',id: 5 },
  { img: mask,inv: 'מלאי נוכחי - 2343', desc: 'מסכות חדשות לקורונה קונספירציה וכדומה', label: 'מסכות',id: 6 },
  { img: shoes,inv: 'Entypo_print', desc: 'Print', label: 'Print',id: 7 },
  { rows:[
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
    {colorCode: '001',colorDesc: 'שחור', img: shirt},
  ],img: shirt,inv: 'מלאי נוכחי - 24213', desc: 'חולצות שונות במידות שונות מבד שונה', label: 'חולצות',id: 8 },
  { img: mask,inv: 'FontAwesome5_store', desc: 'Store', label: 'Store',id: 9 },
  { img: hat,inv: 'מלאי נוכחי - 40230', desc: 'כובים במידות שונות בלי קשר לכלום', label: 'כובעים',id: 10 },
  { img: shirt,inv: 'מלאי נוכחי - 24213', desc: 'חולצות שונות במידות שונות מבד שונה', label: 'חולצות',id: 11 },
  { img: shoes,inv: 'Feather_settings', desc: 'Settings', label: 'Settings',id: 12 },
  { img: hat,inv: 'מלאי נוכחי - 40230', desc: 'כובים במידות שונות בלי קשר לכלום', label: 'כובעים',id: 13 },
  { img: shirt,inv: 'מלאי נוכחי - 24213', desc: 'חולצות שונות במידות שונות מבד שונה', label: 'חולצות',id: 14 },  
]; 

const keyboard = [
  { label: '1', id: 1 },
  { label: '2',id: 2 },
  { label: '3',id: 3 },
  { label: '4',id: 4 },
  { label: '5',id: 5 },
  { label: '6',id: 6 },
  { label: '7',id: 7 },
  { label: '8',id: 8 },
  { label: '9',id: 9 },
  { label: '0',id: 10 },
  { label: '-',id: 11 },
  { label: '+',id: 12 },
  { label: '*',id: 13 },
  { label: 'de',id: 14 },  
]; 

function Footer(){ 
  return(
    <FooterView background={`#e9e6e8`}>
      <FooterChild> 
        {keyboard.map((key)=>{
          return <KeyboardItem>
                  <KeyboardItemTitle>
                    {key.label === 'de' ? <FiDelete/> : key.label}
                  </KeyboardItemTitle>
                </KeyboardItem>
        })}
      </FooterChild>
      <FooterChild> 
        <BiCart style={{cursor: 'pointer',right:10, position:'absolute'}} size={30}/>
        <FooterTotalView>
            Total: 0.00
        </FooterTotalView>
      </FooterChild>
    </FooterView>
  )
}

function Table(){ 
  const [searchValue,setSearchValue] = useState('')
  const [options,setOptions] = useState(null) 
  const navigate = useNavigate(); 
  const navigateToHome = () => {  
    navigate('/')
  };  

  useEffect(()=>{
    if(!options){
      let opt: any = json.sort(function(a, b) {
        let nameA = a.label.toUpperCase(); 
        let nameB = b.label.toUpperCase();   
        return nameA < nameB ? 1 : nameA > nameB ? -1 : 0;
      });
      setOptions(opt)
    } 
  },[]) 

  const refSizeHeader: any = useRef(null);
  const refSizeTable = useRef(null);

  const { onMouseDown } = useDraggableScroll(refSizeTable);
    function update() {
      // const container: any = document.getElementById("controls");
      // const elem: any = document.getElementById("example");
      const rect = refSizeHeader.getBoundingClientRect();
      const scrollTop = document.documentElement.scrollTop;
    
      // container.innerHTML = '';
      // for (let key in rect) {
      //   if(typeof rect[key] !== 'function') {
      //     let para = document.createElement('p');
      //     para.textContent  = `${ key } : ${ rect[key] }`;
      //     container.appendChild(para);
      //   }
      // }
    }
  return(
    <MainView id={`main-view`}>
        <Header mode={`table`} navigateToHome={navigateToHome} {...{options, setSearchValue, searchValue}}/>   
          <TableView> 
            <TableHeader>
              <TableInfoColHeader>
                <TableInfoColHeaderChild> 
                 <p> {'תמונה'} </p> 
                 <p> {'תאור צבע'} </p>
                 <p> {'קוד צבע'} </p>
                </TableInfoColHeaderChild>
                <TableInfoColHeaderChild content={`center`} background={`#6f6b6f`}>
                    {'Total: 0'}
                </TableInfoColHeaderChild>
              </TableInfoColHeader>
              <TableInfoColHeader ref={refSizeHeader} overflow width={`calc(100% - 250px)`}> 
                <TableSizeHeader className={'table-size-header'}>
                  <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'XS'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader>  
                    <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'S'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader> 
                    <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'M'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader> 
                    <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'L'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader> 
                    <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'XL'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader> 
                    <TableInfoColHeader>
                    <TableInfoColHeaderChild> 
                    <p> {'2XL'} </p>  
                    </TableInfoColHeaderChild> 
                  </TableInfoColHeader> 
                </TableSizeHeader>
              </TableInfoColHeader>  
            </TableHeader>    
            <TableScroll>
            <TableInfoCol> 
              <TableInfoColView>
              {(json[7].rows ? json[7].rows : []).map((opt: any, index: number)=>{
                return <TableInfoColChild>
                  {opt.img ? <img height={40} width={40} src={opt.img} alt={opt.label}/> : null}
                  {opt.colorDesc} 
                 <p> {opt.colorCode} </p>
                </TableInfoColChild>
              })} 
              </TableInfoColView>
            </TableInfoCol>
            <TableInfoCol onScroll={(e: any)=>{
              console.log('onScroll',e.target.scrollLeft)
              if(refSizeHeader && refSizeHeader.current){ 
                const container: any = findDOMNode(refSizeHeader.current);
                const scrollInner = container.querySelector('div');
                // const el:any = document.querySelector('.table-size-header');
                // el.scrollLeft = e.target.scrollLeft;
                scrollInner.scrollLeft = e.target.scrollLeft;
                refSizeHeader.current.scrollLeft = e.target.scrollLeft; 
                console.log('onScroll',refSizeHeader.current.scrollLeft, scrollInner.scrollLeft)
              }
            }} ref={refSizeTable} onMouseDown={onMouseDown} id={`table-scroll`} overflow={`auto`} background={`red`} width={`calc(100% - 250px)`}> 
              <TableInfoColView width={`1500px`}>
              {(json[7].rows ? json[7].rows : []).map((opt: any, index: number)=>{
                return <TableSizeHeader maxHeight={`50px`} className={`table-size-header`}>
                <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'XS'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader>  
                  <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'S'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader> 
                  <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'M'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader> 
                  <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'L'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader> 
                  <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'XL'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader> 
                  <TableInfoColHeader>
                  <TableInfoColHeaderChild> 
                  <p> {'2XL'} </p>  
                  </TableInfoColHeaderChild> 
                </TableInfoColHeader> 
              </TableSizeHeader>
              })} 
              </TableInfoColView>
            </TableInfoCol>
        </TableScroll>  
          </TableView>
      <Footer />
    </MainView>
  )
}

function Main(){ 
  const [searchValue,setSearchValue] = useState('')
  const [options,setOptions] = useState(null) 
  const navigate = useNavigate(); 
  const navigateToTable = () => {  
    navigate('/table',{
      state: {
        rows: {
          teafa: 'asd'
        }
      }
    })
  };  

  useEffect(()=>{
    if(!options){
      let opt: any = json.sort(function(a, b) {
        let nameA = a.label.toUpperCase(); 
        let nameB = b.label.toUpperCase();   
        return nameA < nameB ? 1 : nameA > nameB ? -1 : 0;
      });
      setOptions(opt)
    } 
  },[])

  return(
    <MainView id={`main-view`}>
      <Header {...{options, setSearchValue, searchValue}}/> 
      <ViewContainer> 
        <GridView id={`grid-view`}>
          {(options ? options : json).map((opt,index: number)=>{
            if(opt.label.toString().includes(searchValue)){  
              return(
                <ItemView onClick={navigateToTable} key={opt.id}>  
                    {opt.img ? <img height={100} width={100} src={opt.img} alt={opt.label}/> : null}
                    <IconTitle color={`#000`}>{opt.label}</IconTitle>
                    <IconTitle color={`#000`}>{opt.desc}</IconTitle>
                    <IconTitle color={`#000`}>{opt.inv}</IconTitle>
                </ItemView>
              )
            }
            return null
          })}
        </GridView>
      </ViewContainer> 
    </MainView>
  )
}

function Header(props: any){
  const { options, setSearchValue, searchValue, mode, navigateToHome} = props;
  return(
    <HeaderView background={`#8bc313`}>
      <HeaderChild>
        {navigateToHome && <HeaderChildItem width={`33.33%`}> 
          <IoIosArrowBack onClick={navigateToHome ? navigateToHome : null} style={{cursor: 'pointer',color:'#fff'}} size={30}/>
        </HeaderChildItem>}
        <HeaderChildItem direction={`column`} content={`center`} width={navigateToHome ? `33.33%` : `100%`}> 
          <IconTitle size={`22px`}>
            Sales Order
          </IconTitle>
          <IconTitle size={`16px`}>
            כותרת לא ברורה שקשה לקרוא מספר 45024
          </IconTitle>
        </HeaderChildItem> 
      </HeaderChild>
      {mode !== 'table' || !options ? <HeaderChild background={`#fff`}>
        <HeaderChildItem width={`85%`}>
          <Autocomplete
              style={{minWidth:`100%`,backgroundColor:`#e9e6e8`}}
              disablePortal
              id="combo-box-demo"
              options={options ? options : json}
              sx={{ width: 300 }} 
              onSelect={(e: any)=>{  
                let opt = options ? options : json
                if(e.target.value === '' && searchValue !== '')
                  setSearchValue('')
                else
                  for(let i = 0;i < opt.length; i++)
                    if(opt[i].label === e.target.value){
                      setSearchValue(opt[i].desc)  
                      break;
                    } 
              }} 
              renderInput={(params: any) => <TextField onChange={(e: any)=>{ 
                  setSearchValue(e.target.value) 
              }} {...params} label="Search" />}
            />  
        </HeaderChildItem>
        <HeaderChildItem width={`15%`}>
                <BiFilterAlt style={{cursor: 'pointer'}} size={30}/>
                <BiImage style={{cursor: 'pointer'}} size={30}/>
                <BiSort style={{cursor: 'pointer'}} size={30}/>
        </HeaderChildItem>
        </HeaderChild> : 
        <HeaderChild background={`#fff`}>
          {options.map((opt: any, index: number)=>{
              return <HeaderChildImg background={opt.id === 8 ? `#cecece` : null}>
                {opt.img ? <img height={65} width={65} src={opt.img} alt={opt.label}/> : null}
              </HeaderChildImg>
            })}
        </HeaderChild>}
    </HeaderView>
  )
}

function App() {
  return (
    <div className="App">
      <Router> 
        <Routes> 
          <Route path="/table" element={<Table/>}/>
          <Route path="/" element={<Main/>}/>
        </Routes> 
      </Router>
    </div>
  );
}

export default App;
